
package rigolcontrol;

import java.net.*;
import java.io.*;

/**
 * Offers basic control of Rigol DS1000Z series oscilloscopes over the network.
 * The only thing that this main class does is call up the MainWindow.
 * Released as Public Domain.
 * 
 * @author Jake Moomaw
 * 
 */
public class RigolControl {
    /**
     * The only thing that main does is call the Main Window.
     * @param args completely ignored
     */
 
    public static void main(String[] args) {
        String[] test = {""};
        MainWindow.main(test);
        
    }
    
        
}
    
