/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rigolcontrol;

import static rigolcontrol.RigolComm.sendReceiveComm;

/**
 * Provides all of the time functionality of the Rigol Oscilloscope.
 * @todo add mode to the update function.
 * @author Jake Moomaw
 */
public class ScopeTime {
    private enum TimeMode {MAIN, XY, ROLL}
    private boolean sweepDelayEnabled;
    private double delayOffset;
    private String delayOffsetFormat;
    private double delayScale;
    private String delayScaleFormat;
    private double offset;
    private String offsetFormat;
    private double scale;
    private String scaleFormat;
    private TimeMode mode;
    
    /**
     * Basic constructor.  Sets all of the member variables to the defaults
     * for the scope.
     */
    public ScopeTime() {
        sweepDelayEnabled = false;
        delayOffset = 0;
        delayOffsetFormat = "0s";
        delayScale = .0000005;
        delayScaleFormat = "5ns";
        offset = 0;
        offsetFormat = "0s";
        scale = .000001;
        scaleFormat = "1µs";
        mode = TimeMode.MAIN;
    }
    
    /**
     * Increases the time scale in the stepwise 1-2-5 factor that the dial on
     * the front of the scope uses.
     * @param ip Address of the scope.
     */
    
    public void increaseScale(String ip) {
        String scaleString = String.valueOf(scale);
        if (scaleString.charAt(0) == '2') {
            scale = scale * 2.5;
        }
        else {
            scale = scale * 2;
        }
        for (int i = 0; i < 20; i++) {
            RigolComm.sendSimpleComm(ip, "TIM:MAIN:SCAL " + 
                        String.valueOf(scale));
        }
        scale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:SCAL?"));
        scaleFormat = formatTime(scale);
    }
    
    /**
     * Decreases the time scale in the stepwise 1-2-5 factor that the dial on
     * the front of the scope uses.
     * @param ip Address of the scope.
     */
    public void decreaseScale(String ip) {
        String scaleString = String.valueOf(scale);
        if (scaleString.charAt(0) == '5') {
            scale = scale / 2.5;
        }
        else {
            scale = scale / 2;
        }
        for (int i = 0; i < 20; i++) {
            RigolComm.sendSimpleComm(ip, "TIM:MAIN:SCAL " + 
                        String.valueOf(scale));
        }
        scale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:SCAL?"));
        scaleFormat = formatTime(scale);
    }
    
    public void changeOffset(String ip, double change) {
        scale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:SCAL?"));
        offset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:OFFS?"));
        offset += scale * change;
        for (int i = 0; i < 20; i++) {
            RigolComm.sendSimpleComm(ip, "TIM:MAIN:OFFS " + 
                        String.valueOf(offset));
        }
        offset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:OFFS?"));
        offsetFormat = formatOffset(offset);
    }
    
    /**
     * Generates the string to be displayed by the interface's time scale.
     * @param rawTimeNum The object's scale or delayScale value
     * @return String for time scale display
     */
    private String formatTime(double rawTimeNum) {
        if (rawTimeNum < .000001) {
            return (String.valueOf((int)(rawTimeNum / .000000001))+"ns" );
        }
        else if (rawTimeNum < .001) {
            return (String.valueOf((int)(rawTimeNum / .000001))+"µs");
        }
        else if (rawTimeNum < 1) {
            return (String.valueOf((int)(rawTimeNum / .001))+"ms");
        }
        else {
            return (String.valueOf((int)(rawTimeNum))+"s");
        }
    }
    
    /**
     * Generates the string to be displayed by the interface's time offset.
     * @param rawTimeNum The object's offset or delayOffset value
     * @return String for time offset display
     */    
    private String formatOffset(double rawTimeNum) {
        if (Math.abs(rawTimeNum) < .000001) {
            return (String.valueOf((int)(rawTimeNum / .000000001))+"ns" );
        }
        else if (Math.abs(rawTimeNum) < .001) {
            return (String.valueOf((int)(rawTimeNum / .000001))+"µs");
        }
        else if (Math.abs(rawTimeNum) < 1) {
            return (String.valueOf((int)(rawTimeNum / .001))+"ms");
        }
        else {
            return (String.valueOf((int)(rawTimeNum))+"s");
        }
    }
    
    /**
     * Provides access to the scaleFormat value
     * @return scaleFormat value
     */
    public String getScaleFormat() {
        return scaleFormat;
    }
    
    /**
     * Provides access to the offsetFormat value
     * @return offsetFormat value
     */
    public String getOffsetFormat() {
        return offsetFormat;
    }
    
    /**
     * Updates all of the object's member variables with the actual values from
     * the scope.  Should be used at beginning of program and as a sanity
     * check.
     * @param ip Ip address of the scope
     */
    public void update(String ip) {
        if (RigolComm.sendReceiveComm(ip, "TIM:DEL:ENAB?") == "0") {
            sweepDelayEnabled = false;
        }
        else {
            sweepDelayEnabled = true;
        }
        delayOffset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:DEL:OFFS?"));
        delayOffsetFormat = formatOffset(delayOffset);
        delayScale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:DEL:SCAL?"));
        delayScaleFormat = formatTime(delayScale);
        offset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:OFFS?"));
        offsetFormat = formatOffset(offset);
        scale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "TIM:MAIN:SCAL?"));
        scaleFormat = formatTime(scale);
        mode = TimeMode.MAIN;
    }
}
