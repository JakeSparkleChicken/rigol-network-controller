/*
 * Public Domain
 */
package rigolcontrol;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.Socket;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.ImageIcon;
import java.nio.ByteBuffer;
import javax.imageio.ImageIO;


/**
 * This is the class that has all of the network commands for communicating
 * with the Rigol DS1000Z series oscilloscopes.  It also has some formatting
 * commands for time and voltage numbers, in addition to generating an 
 * ImageIcon for displaying screen captures.  A full list of the available 
 * commands can be found in the MSO1000Z/DS1000Z Series Digital Oscilloscope
 * Programming Guide, https://beyondmeasure.rigoltech.com/acton/attachment/
 *                  1579/f-0386/1/-/-/-/-/DS1000Z_Programming%20Guide_EN.pdf
 * @author Jake Moomaw
 */
public class RigolComm {
    public static void main(String ip, String cmd) {
        try (Socket socket = new Socket(ip, 5555)) {
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), 
                        true);) {
                out.println(cmd);
                out.flush();
            }
            catch (Exception e) {
                System.err.println(e);
            }
        }
        catch (Exception e) {
            System.err.println(e);
        }
    }
    /**
     * This method is used to send commands that you don't expect to get
     * any data from afterwards.  Things like Auto range and setting a specific
     * range fall into this category.  
     * @param ip  IP address of the oscilloscope.
     * @param cmd  The command being sent to the oscilloscope.
     * @return  Returns bollean true if the command succeeds, false if it does
     * not.
     */
    public static boolean sendSimpleComm(String ip, String cmd) {
        try (Socket socket = new Socket(ip, 5555)) {
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), 
                        true);) {
                out.println(cmd);
                out.flush();
                return true;
            }
            catch (Exception e) {
                System.err.println(e);
                return false;
            }
        }
        catch (Exception e) {
            System.err.println(e);
            return false;
        }
    }
    /**
     * This method is to send commands that you expect data to be returned from.
     * Reading the current time scale or voltage range are examples of this.
     * @param ip  The IP address of the oscilloscope.
     * @param cmd  The command being sent.
     * @return  A string object containing the data from the oscilloscope.
     */
    public static String sendReceiveComm(String ip, String cmd) {
        String input = "";
        try (Socket socket = new Socket(ip, 5555)) {
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), 
                        true);) {
                out.println(cmd);
                out.flush();
                try (BufferedReader in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()))) {
                    input = in.readLine();
                }
                catch (Exception e) {
                    System.err.println(e);
                }
            }
            catch (Exception e) {
                System.err.println(e);
            }
            
        }
        catch (Exception e) {
            System.err.println(e);
        }
        return input;
    }
    /**
     * This method is used for obtaining a screen capture from the
     * oscilloscope in JPEG format as an ImageIcon.
     * @param ip  IP address of the oscilloscope.
     * @return  An ImageIcon object containing the screen capture.
     */
    public static ImageIcon getPNG(String ip) {
        byte[] png = {};
        try (Socket socket = new Socket(ip, 5555)) {
            try (PrintWriter out = new PrintWriter(socket.getOutputStream(), 
                        true);) {
                out.println("DISP:DATA? ON,0,BMP8");
                out.flush();
                try {
                    DataInputStream in = new
                        DataInputStream(socket.getInputStream());
                    int bytesRead = 0;
                    byte trash = in.readByte();
                    trash = in.readByte();
                    String imageLength ="";
                    for (int i = 0; i<9 ; i++) {
                        imageLength = imageLength + (char)in.readByte();  
                    }
                    png = new byte[Integer.parseInt(imageLength)];
                    ByteBuffer byteBuffer = ByteBuffer.wrap(png, bytesRead,
                            png.length);
                    while (bytesRead < png.length) {
                        png[bytesRead] = in.readByte();
                        bytesRead++;
                    }
                    
                }
                catch (Exception e) {
                    System.err.println(e);
                }
               
            }
            catch (Exception e) {
                System.err.println(e);
            }
            
        }
        catch (Exception e) {
            System.err.println(e);
        }
        try {
            BufferedImage in = ImageIO.read(new ByteArrayInputStream(png));

            return new ImageIcon(in);
        }
        catch (Exception e) {
            System.out.println(e);
        }
        return new ImageIcon();
    }
    
   /**
    * This method converts raw time scale information from the oscilloscope
    * into something more human readable.  For example, .00000001 would return
    * 10ns.
    * @param ip  IP address of the oscilloscope.
    * @return  String object containing the ruman readable time value.
    */
    public static String getTimeFormat(String ip) {
        double rawTimeNum = Double.parseDouble(sendReceiveComm(ip, 
                "TIM:MAIN:SCAL?"));
        if (rawTimeNum < .000001) {
            return (String.valueOf((int)(rawTimeNum / .000000001))+"ns" );
        }
        else if (rawTimeNum < .001) {
            return (String.valueOf((int)(rawTimeNum / .000001))+"µs");
        }
        else if (rawTimeNum < 1) {
            return (String.valueOf((int)(rawTimeNum / .001))+"ms");
        }
        else {
            return (String.valueOf((int)(rawTimeNum))+"s");
        }
    }
    
    public static String getOffsetTimeFormat(String ip) {
        double rawTimeNum = Double.parseDouble(sendReceiveComm(ip, 
                "TIM:MAIN:OFFS?"));
        if (Math.abs(rawTimeNum) < .000001) {
            return (String.valueOf((int)(rawTimeNum / .000000001))+"ns" );
        }
        else if (Math.abs(rawTimeNum) < .001) {
            return (String.valueOf((int)(rawTimeNum / .000001))+"µs");
        }
        else if (Math.abs(rawTimeNum) < 1) {
            return (String.valueOf((int)(rawTimeNum / .001))+"ms");
        }
        else {
            return (String.valueOf((int)(rawTimeNum))+"s");
        }
    }
   /**
    * This method converts raw voltage range information from the oscilloscope
    * into something more human readable.  For example, .01 would return
    * 10mV.
    * @param ip  IP address of the oscilloscope.
    * @return  String object containing the ruman readable voltage value.
    */
    public static String getVoltFormat(String ip, char chan) {
        double rawVoltNum = Double.parseDouble(sendReceiveComm(ip, "CHAN"+chan+
                ":SCAL?"));
        if (rawVoltNum < 1) {
            return (String.valueOf((int)(rawVoltNum / .001))+"mV" );
        }
        
        else {
            return (String.valueOf((int)(rawVoltNum))+"V");
        }
    }
    public static String formatRange(double rawVoltNum){
        if (rawVoltNum < 1) {
            return (String.valueOf((int)(rawVoltNum / .001))+"mV" );
        }
        
        else {
            return (String.valueOf((int)(rawVoltNum))+"V");
        }
    }
    
    public static String formatOffset(double rawVoltNum){
        if (Math.abs(rawVoltNum) < .001) {
            return (String.valueOf((rawVoltNum / .000001))+"uV" );
        }
        if (Math.abs(rawVoltNum)< 1) {
            return (String.valueOf((rawVoltNum / .001))+"mV" );
        }
        else {
            return (String.valueOf((rawVoltNum))+"V");
        }
    }
}
