GUI program for controlling Rigol DS1000Z series oscilloscopes over the
network.  Currently, it is only a proof of concept that I've cobbled together
as a means of learning networking and interface design with Java since I'm
learning it for school.