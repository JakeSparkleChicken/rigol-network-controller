package rigolcontrol;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class RecordAndPlayWindow extends JFrame{
    private JLabel frameNumberField;
    private JButton recordButton;
    private JButton frameReverseCourseButton;
    private JButton frameReverseFineButton;
    private JButton frameForwardFineButton;
    private JButton frameForwardCourseButton;
    private String ip;
    private MainWindow mainWindow;

    public RecordAndPlayWindow(String ip, MainWindow mainWindow) {
        RigolComm.sendSimpleComm(ip, "FUNC:WREC:ENAB 1");
        this.mainWindow = mainWindow;
        setSize(300, 300);
        setLocation(600, 100);
        this.ip = ip;
        JPanel panel = new JPanel();
        this.setContentPane(panel);
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        recordButton = new JButton("Record");
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 4;
        panel.add(recordButton, c);
        recordButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordButtonActionPerformed(evt);
            }
        });
        frameReverseCourseButton = new JButton("<<");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        //c.gridx = 0;
        frameReverseCourseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });
        panel.add(frameReverseCourseButton, c);
        frameReverseFineButton = new JButton("<");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        //c.gridx = 1;
        frameReverseFineButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });
        panel.add(frameReverseFineButton, c);
        frameForwardFineButton = new JButton(">");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        //c.gridx = 2;
        frameForwardFineButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });
        panel.add(frameForwardFineButton, c);
        frameForwardCourseButton = new JButton(">>");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridy = 2;
        //c.gridx = 3;
        frameForwardCourseButton.addActionListener(new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });
        panel.add(frameForwardCourseButton, c);
        frameNumberField = new JLabel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridwidth = 4;
        c.gridy = 0;
        panel.add(frameNumberField, c);
        this.setVisible(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                mainWindow.recordAndPlay.setEnabled(true);
                RigolComm.sendSimpleComm(ip, "FUNC:WREC:ENAB 0");
                try {
                    Thread.sleep(100);
                }
                catch (Exception e) {}
                RigolComm.sendSimpleComm(ip, "RUN");
                mainWindow.updateScreenButtonActionPerformed();
            }
        });
    }

    private void recordButtonActionPerformed(ActionEvent evt) {

        RigolComm.sendSimpleComm(ip, "FUNC:WREC:OPER RUN");
        mainWindow.updateScreenButtonActionPerformed();
    }


    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {
        int frame;
        RigolComm.sendSimpleComm(ip, "FUNC:WREP:FST");
        switch (evt.getActionCommand()) {
            case ">>":
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:DIR FORW");
                frame = Integer.parseInt(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:FCUR " + (frame + 10));
                mainWindow.updateScreenButtonActionPerformed();
                frameNumberField.setText(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                break;
            case ">":
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:DIR FORW");
                frame = Integer.parseInt(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:FCUR " + (frame + 1));
                frameNumberField.setText(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                mainWindow.updateScreenButtonActionPerformed();
                break;
            case "<<":
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:DIR BACK");
                frame = Integer.parseInt(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:FCUR " + (frame - 10));
                frameNumberField.setText(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                mainWindow.updateScreenButtonActionPerformed();
                break;
            case "<":
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:DIR BACK");
                frame = Integer.parseInt(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                RigolComm.sendSimpleComm(ip, "FUNC:WREP:FCUR " + (frame - 1));
                frameNumberField.setText(RigolComm.sendReceiveComm(ip, "FUNC:WREP:FCUR?"));
                mainWindow.updateScreenButtonActionPerformed();
                break;
        }
    }


}
