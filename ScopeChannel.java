/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rigolcontrol;

/**
 * Class for ScopeChannel objects.  On creation, channels have the default
 * settings for the Rigol oscilloscopes.  Deeper information on the settings
 * can be found in the DS1000Z Programming Guide.
 * @todo Move formatting from RigolComm
 * @author Jake Moomaw
 */
public class ScopeChannel {
    private enum Unit {VOLT, WATT, AMP, UNKN};
    private enum Coupling {AC, DC, GND};
    private boolean enabled;
    private int number;
    private Coupling coupled;
    private double range;
    private boolean bwLimited;
    private boolean inverted;
    private double offset;
    private double timeCalDelay;
    private double scale;
    private double probeRatio;
    private Unit units;
    private boolean vernier;
    private String rangeFormat;
    private String offsetFormat;
    /**
     * Constructor for ScopeChannel.  Sets everything to the defaults as
     * defined in the DS1000Z Programming Guide.
     * @param id The Channel Number.  Must be between 1 and 4, inclusive.
     */
    public ScopeChannel(int id) {
        if (id < 1 || id > 4) {
            throw new IllegalArgumentException("Channel Number must be between"+
                    "1 and 4!");
        }
        number = id;
        bwLimited = false;
        coupled = Coupling.DC;
        if (id == 1) {
            enabled = true;
        }
        else {
            enabled = false;
        }
        inverted = false;
        offset = 0;
        range = 8;
        timeCalDelay = 0;
        scale = 8;
        probeRatio = 10;
        units = Unit.VOLT;
        vernier = false;
        rangeFormat = "8V";
        offsetFormat = "0V";
    }
    /**
     * Changes the range on the channel by multiplying the current range value
     * by mult.  It sends the command 20 times since the scope occasionally
     * will not behave well.
     * @param ip  The IP address of the oscilloscope
     * @param mult  The multiplier for the range change
     */
    public void changeRange(String ip, double mult) {
        range = range*mult;
        for (int i = 0; i < 20; i++) {
            RigolComm.sendSimpleComm(ip, "CHAN" + number + ":RANG " + 
                    String.valueOf(range));
        }
        range = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":RANG?"));
        rangeFormat =  RigolComm.formatRange(range);        
    }
    
    public void changeOffset(String ip, double mult) {
        offset += range*mult ;
        for (int i = 0; i < 20; i++) {
            RigolComm.sendSimpleComm(ip, "CHAN" + number + ":OFFS " + 
                    String.valueOf(offset));
        }
        offset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":OFFS?"));
        offsetFormat =  RigolComm.formatOffset(offset);        
    }
    
    public String getRangeFormat() {
        return rangeFormat;
    }
    
    public String getOffsetFormat() {
        return offsetFormat;
    }
    /**
     * Refreshes the channel's variables with the values that the oscilloscope
     * is actually using.  Use this as soon as the program starts, or as a
     * sanity check.
     * @param ip  The IP address of the oscilloscope
     */
    public void update(String ip) {
        range = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":RANG?"));
        rangeFormat = RigolComm.formatRange(range);
        if (RigolComm.sendReceiveComm(ip, "CHAN" + number + ":BWL?") == "OFF") {
            bwLimited = false;
        }
        else {
            bwLimited = true;
        }
        if (RigolComm.sendReceiveComm(ip, "CHAN" + number + ":DISP?") == "1") {
            enabled = true;
        }
        else {
            enabled = false;
        }
        if (RigolComm.sendReceiveComm(ip, "CHAN" + number + ":INV?") == "1") {
            inverted = true;
        }
        else {
            inverted = false;
        }
        offset = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":OFFS?"));
        offsetFormat = RigolComm.formatOffset(offset);
        timeCalDelay = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":TCAL?"));
        scale = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":SCAL?"));
        probeRatio = Double.parseDouble(RigolComm.sendReceiveComm(ip, "CHAN" +
                number + ":PROB?"));
        if (RigolComm.sendReceiveComm(ip, "CHAN" + number + ":VERN?") == "1") {
            vernier = true;
        }
        else {
            vernier = false;
        }
    }
}
